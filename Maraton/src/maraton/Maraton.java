package maraton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class Maraton {
	private int[] podio = new int[10];
	
	public Maraton(String f) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(f));
		
		int cantidadDeCorredores=sc.nextInt();
				
		for (int i = 0; i < cantidadDeCorredores ; i++) {
			int corredor=sc.nextInt();
			int categoria=sc.nextInt();
			if (this.podio[categoria-1]==0)
				this.podio[categoria-1]=corredor;
		}
		sc.close();

	}
	
	public void imprimir(String s) throws IOException {
		PrintWriter salida = new PrintWriter(new FileWriter(s));
		for(int i=0; i<this.podio.length; i++)			
			salida.println(i+1 +" "+ this.podio[i]);
		salida.close();		
	}

	public static void main(String[] args) throws IOException {
		Maraton m = new Maraton("carrera.in");
		m.imprimir("podio.out");
		
	}

}
